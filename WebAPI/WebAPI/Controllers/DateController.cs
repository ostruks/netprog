﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class DateController : ApiController
    {
        //GET api/date
        // GET api/date
        public string Get()
        {
            return DateTime.Now.ToString();
        }
    }
}
