﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.SelfHost;

namespace SelfHostingWithRoutingAndControllers
{
    class Program
    {
        static void Main(string[] args)
        {
            string address = "http://localhost:63467";
            var config = new HttpSelfHostConfiguration(address);
            config.Routes.MapHttpRoute(
                "MyRoute",
                "api/{controller}/{mail}",
                new { id = RouteParameter.Optional });
            var server = new HttpSelfHostServer(config);
            server.OpenAsync();
            Console.WriteLine("Сервис доступен по адресу {0}/api/my", address);
            Console.ReadKey();
        }
    }
}
