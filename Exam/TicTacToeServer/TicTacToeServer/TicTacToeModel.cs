﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeServer
{
    public class TicTacToeModel
    {
        private string[,] matrix;
        private List<string> X = new List<string>(3);
        private List<string> O = new List<string>(3);
        public string XVAlue = "X";
        public string OValue = "O";
        string EmptyValue = "-";
        public TicTacToeModel()
        {
            matrix = new string[3, 3]
            {
                { EmptyValue, EmptyValue, EmptyValue },
                { EmptyValue, EmptyValue, EmptyValue },
                { EmptyValue, EmptyValue, EmptyValue }
            };
        }
        public string haveLine()
        {
            if ((matrix[0, 0] == XVAlue && matrix[1, 0] == XVAlue && matrix[2, 0] == XVAlue) ||
                (matrix[0, 1] == XVAlue && matrix[1, 1] == XVAlue && matrix[2, 1] == XVAlue) ||
                (matrix[0, 2] == XVAlue && matrix[1, 2] == XVAlue && matrix[2, 2] == XVAlue) ||
                (matrix[0, 0] == XVAlue && matrix[0, 1] == XVAlue && matrix[0, 2] == XVAlue) ||
                (matrix[1, 0] == XVAlue && matrix[1, 1] == XVAlue && matrix[1, 2] == XVAlue) ||
                (matrix[2, 0] == XVAlue && matrix[2, 1] == XVAlue && matrix[2, 2] == XVAlue) ||
                (matrix[0, 0] == XVAlue && matrix[1, 1] == XVAlue && matrix[2, 2] == XVAlue) ||
                (matrix[2, 0] == XVAlue && matrix[1, 1] == XVAlue && matrix[0, 2] == XVAlue))
            {
                return "X";
            }
            if ((matrix[0, 0] == OValue && matrix[1, 0] == OValue && matrix[2, 0] == OValue) ||
                (matrix[0, 1] == OValue && matrix[1, 1] == OValue && matrix[2, 1] == OValue) ||
                (matrix[0, 2] == OValue && matrix[1, 2] == OValue && matrix[2, 2] == OValue) ||
                (matrix[0, 0] == OValue && matrix[0, 1] == OValue && matrix[0, 2] == OValue) ||
                (matrix[1, 0] == OValue && matrix[1, 1] == OValue && matrix[1, 2] == OValue) ||
                (matrix[2, 0] == OValue && matrix[2, 1] == OValue && matrix[2, 2] == OValue) ||
                (matrix[0, 0] == OValue && matrix[1, 1] == OValue && matrix[2, 2] == OValue) ||
                (matrix[2, 0] == OValue && matrix[1, 1] == OValue && matrix[0, 2] == OValue))
            {
                return "O";
            }
            return "N";
        }
        public bool haveValue(int iposition, int jposition)
        {
            string curValue = matrix[iposition, jposition];
            bool result = curValue.Contains(EmptyValue);
            if (result == false)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void setValue(string value, int iposition, int jposition)
        {
            matrix[iposition, jposition] = value;
        }
    }
}
