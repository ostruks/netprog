﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeServer
{
    class Program
    {
        public static List<Player> players = new List<Player>();
        private static TcpListener serverSocket;
        private static NetworkStream networkStream;
        static int port = 55555; // порт для приема входящих запросов
        public static void BroadcastMessage(string msg, int iposition, int jposition, string id)
        {
            try
            {
                byte[] data = Encoding.ASCII.GetBytes($"{msg}:{iposition}:{jposition}");
                for (int i = 0; i < players.Count; i++)
                {
                    if (players[i].Id != id)
                    {
                        players[i].Stream.Write(data, 0, data.Length);
                    }
                    if (players[i].Id == id)
                    {
                        players[i].Stream.Write(data, 0, data.Length);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }          
        }
        static void Main(string[] args)
        {
            Task.Factory.StartNew(startServer);
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
        private static void startServer()
        {
            try
            {
                serverSocket = new TcpListener(IPAddress.Any, port);
                TcpClient clientSocket = default(TcpClient);
                int counter = 0;
                serverSocket.Start();
                Console.WriteLine("Chart Server started...");
                while (true)
                {
                    counter++;
                    clientSocket = serverSocket.AcceptTcpClient();
                    byte[] bytesForm = new byte[1024];
                    string dataFormClient = null;
                    networkStream = clientSocket.GetStream();
                    if ((networkStream.CanRead == false) || (networkStream.DataAvailable == false))
                    {
                        continue;
                    }
                    networkStream.Read(bytesForm, 0, bytesForm.Length);
                    dataFormClient = System.Text.Encoding.ASCII.GetString(bytesForm);
                    dataFormClient = dataFormClient.Substring(0, dataFormClient.LastIndexOf("$"));
                    Player player = new Player();
                    Console.WriteLine(dataFormClient.Split(':')[0] + " Joined chart room");
                    players.Add(player);
                    player.startPlayer(clientSocket, dataFormClient.Split(':')[0]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
          
        }
    }
}
