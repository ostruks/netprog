﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeServer
{
    public sealed class TicTacToeController
    {
        private TicTacToeModel model = new TicTacToeModel();
        private Player _player;
        public TicTacToeController(Player player)
        {
            _player = player;
        }
        public bool sendPosition(int iposition, int jposition, out string viewValue)
        {
            if (!model.haveValue(iposition, jposition))
            {
                if (_player.PlayerName == "Player 1")
                {
                    viewValue = model.XVAlue;
                    model.setValue(model.XVAlue, iposition, jposition);
                }
                else
                {                    
                    viewValue = model.OValue;
                    model.setValue(model.OValue, iposition, jposition);
                }
                if (model.haveLine() == "X")
                {
                    viewValue = "X Wins!";
                }
                if (model.haveLine() == "O")
                {
                    viewValue = "O Wins!";
                }
                return true;
            }
            else
            {
                viewValue = String.Empty;
                return false;
            }
        }
    }
}
