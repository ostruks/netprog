﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TicTacToeServer
{
    public class Player
    {
        private TicTacToeController controller;
        private TcpClient clientSocket;
        private string playerName;
        private string id;
        private NetworkStream stream;
        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
        public NetworkStream Stream
        {
            get
            {
                return stream;
            }

            set
            {
                stream = value;
            }
        }
        public string PlayerName
        {
            get
            {
                return playerName;
            }

            set
            {
                playerName = value;
            }
        }
        public TcpClient ClientSocket
        {
            get
            {
                return clientSocket;
            }

            set
            {
                clientSocket = value;
            }
        }

        public void startPlayer(TcpClient ClientSocket, string playerName)
        {
            this.id = Guid.NewGuid().ToString();
            this.clientSocket = ClientSocket;
            this.playerName = playerName;
            Task ctThread = new Task(letsGame);
            ctThread.Start();
        }
        public Player() { }
        private void letsGame()
        {
            byte[] bytesFrom = new byte[1024];
            string dataFromClient = null;
            while (true)
            {
                try
                {
                    Stream = clientSocket.GetStream();
                    NetworkStream networkStream = clientSocket.GetStream();
                    if (networkStream.CanRead == false || networkStream.DataAvailable == false)
                        continue;
                    networkStream.Read(bytesFrom, 0, bytesFrom.Length);
                    dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
                    controller = new TicTacToeController(this);
                    string viewValue;
                    int iposition = Convert.ToInt32(dataFromClient.Split(':')[1]);
                    int jposition = Convert.ToInt32(dataFromClient.Split(':')[2]);
                    controller.sendPosition(iposition, jposition, out viewValue);
                    Program.BroadcastMessage(viewValue, iposition, jposition, this.id);
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
