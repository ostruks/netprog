   public static void broadcast(string msg, string uName, string friendName, bool flag)
        {
            foreach (DictionaryEntry Item in clientsList)
            {
                TcpClient broadcastSocket;
                broadcastSocket = (TcpClient)Item.Value;               
                try
                {
                    NetworkStream broadcastStream = broadcastSocket.GetStream();
                    Byte[] broadcastBytes = null;
                    if (flag == true)
                    {
                        broadcastBytes = Encoding.ASCII.GetBytes($"{uName} says : {msg}");
                    }
                    else
                    {
                        broadcastBytes = Encoding.ASCII.GetBytes(msg);
                    }
                    broadcastStream.Write(broadcastBytes, 0, broadcastBytes.Length);
                    broadcastStream.Flush();
                }
                catch (Exception) { }
            }           
        }
                    //Program.broadcast(dataFromClient.Split(':')[4], clientName, friendName, true);
