﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SynchronizationContext sinc = SynchronizationContext.Current;
        private Button[,] btnLines;
        private Button start;
        private Button btn;
        private ComboBox playerList;
        StackPanel first_row;
        StackPanel second_row;
        StackPanel third_row;

        private TcpClient clientSocket;
        private NetworkStream serverStream = default(NetworkStream);
        private int ServerPort = 55555;
        private string ServerIp = GetLocalIP();
        private Task ctThread;
        private CancellationTokenSource cancelTokenSource = new CancellationTokenSource();

        public MainWindow()
        {
            InitializeComponent();
            buildGame();
        }
        private static string GetLocalIP()
        {
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "127.0.0.1";
        }
        private void buildGame()
        {
            first_row = new StackPanel();
            first_row.Name = "first_row";
            first_row.Orientation = Orientation.Horizontal;
            first_row.HorizontalAlignment = HorizontalAlignment.Center;
            first_row.VerticalAlignment = VerticalAlignment.Center;
            btnLines = new Button[3, 3];
            for (int i = 0; i < 3; i++)
            {
                btnLines[0, i] = new Button();
                btnLines[0, i].Content = "btn 0" + i.ToString();
                btnLines[0, i].Click += Button_Click;
                btnLines[0, i].Width = 100;
                btnLines[0, i].Height = 100;
                first_row.Children.Add(btnLines[0, i]);
            }
            DockPanel myMenu = new DockPanel();
            myMenu.Name = "myMenu";
            playerList = new ComboBox();
            playerList.Name = "palyerList";
            playerList.Width = 30;
            playerList.Width = 200;
            playerList.Items.Add(new TextBlock() { Text = "Player 1" });
            playerList.Items.Add(new TextBlock() { Text = "Player 2" });
            myMenu.Children.Add(new Label() {Content = "Player: " });
            myMenu.Children.Add(playerList);
            start = new Button();
            start.Name = "start";
            start.Content = "Start";
            start.Click += start_Click;
            myMenu.Children.Add(start);
            TicTacToeField.Children.Add(myMenu);
            TicTacToeField.Children.Add(first_row);
            second_row = new StackPanel();
            second_row.Name = "second_row";
            second_row.Orientation = Orientation.Horizontal;
            second_row.HorizontalAlignment = HorizontalAlignment.Center;
            second_row.VerticalAlignment = VerticalAlignment.Center;
            for (int i = 0; i < 3; i++)
            {
                btnLines[1, i] = new Button();
                btnLines[1, i].Content = "btn 1" + i.ToString();
                btnLines[1, i].Click += Button_Click;
                btnLines[1, i].Width = 100;
                btnLines[1, i].Height = 100;
                second_row.Children.Add(btnLines[1, i]);
            }
            TicTacToeField.Children.Add(second_row);

            third_row = new StackPanel();
            third_row.Name = "second_row";
            third_row.Orientation = Orientation.Horizontal;
            third_row.HorizontalAlignment = HorizontalAlignment.Center;
            third_row.VerticalAlignment = VerticalAlignment.Center;
            for (int i = 0; i < 3; i++)
            {
                btnLines[2, i] = new Button();
                btnLines[2, i].Content = "btn 2" + i.ToString();
                btnLines[2, i].Click += Button_Click;
                btnLines[2, i].Width = 100;
                btnLines[2, i].Height = 100;
                third_row.Children.Add(btnLines[2, i]);
            }
            TicTacToeField.Children.Add(third_row);
            first_row.IsEnabled = false;
            second_row.IsEnabled = false;
            third_row.IsEnabled = false;
        }
        public void sendGameOver(string message)
        {
            MessageBox.Show(message, "Результат игры!", MessageBoxButton.OK, MessageBoxImage.Information);
            TicTacToeField.Children.Clear();
            buildGame();
            first_row.IsEnabled = true;
            second_row.IsEnabled = true;
            third_row.IsEnabled = true;
        }
        private void findView(Button btn, ref int iposition, ref int jposition)
        {
            iposition = -1;
            jposition = -1;
            for (int i = 0; i < 3; i++)
            {
                bool f = false;
                for (int j = 0; j < 3; j++)
                {
                    if (btnLines[i, j] == btn)
                    {
                        iposition = i;
                        jposition = j;
                        f = true;
                        break;
                    }
                }
                if (f) break;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button)
            {
                btn = sender as Button;
                if (btn != null)
                {
                    int iposition = 0;
                    int jposition = 0;
                    findView(btn, ref iposition, ref jposition);
                    byte[] outStream = System.Text.Encoding.ASCII.GetBytes($"{playerList.Text}:{iposition}:{jposition}");
                    serverStream.Write(outStream, 0, outStream.Length);
                    serverStream.Flush();
                }
            }        
        }
        private void start_Click(object sender, RoutedEventArgs e)
        {
            connect();
            playerList.IsEnabled = false;
            start.IsEnabled = false;
            first_row.IsEnabled = true;
            second_row.IsEnabled = true;
            third_row.IsEnabled = true;
        }
        private void connect()
        {
            try
            {
                int port = (playerList.Text == "Player 1") ? 11111 : 22222;
                clientSocket = new TcpClient(new IPEndPoint(IPAddress.Parse(GetLocalIP()), port));
                clientSocket.Connect(ServerIp, ServerPort);
                serverStream = clientSocket.GetStream();
                byte[] outStream = System.Text.Encoding.ASCII.
                    GetBytes($"{playerList.Text}:$");
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
                CancellationToken token = cancelTokenSource.Token;
                ctThread = new Task(() => getMessage(token));
                ctThread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void getMessage(CancellationToken token)
        {
            while (true)
            {
                try
                {
                    if (token.IsCancellationRequested)
                    {
                        MessageBox.Show("Operation is interrupted by a token");
                        return;
                    }
                    serverStream = clientSocket.GetStream();
                    int buffSize = 0;
                    byte[] inStream = new byte[1024];
                    buffSize = clientSocket.ReceiveBufferSize;
                    if (serverStream.CanRead == false || serverStream.DataAvailable == false)
                        continue;
                    serverStream.Read(inStream, 0, inStream.Length);
                    string returndata = System.Text.Encoding.ASCII.GetString(inStream);
                    int i = Convert.ToInt32(returndata.Split(':')[1]);
                    int j = Convert.ToInt32(returndata.Split(':')[2]);
                    sinc.Send(state => {
                        btnLines[i, j].Content = returndata.Split(':')[0];
                        btnLines[i, j].IsEnabled = false; }
                        , null);
                    if ((returndata.Split(':')[0] == "X Wins!") ||
                        (returndata.Split(':')[0] == "O Wins!"))
                    {
                        sinc.Send(state => sendGameOver(returndata.Split(':')[0]), null);
                    }      
                }
                catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
