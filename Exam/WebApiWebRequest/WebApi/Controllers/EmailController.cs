﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web.Http;

namespace WebApi.Controllers
{
    public class EmailController : ApiController
    {
        public HttpResponseMessage Get(string phone)
        {
            MailMessage msg = new MailMessage();
            NameValueCollection appSettings = ConfigurationManager.AppSettings;
            msg.To.Add(phone);
            msg.From = new MailAddress(appSettings["smtpuser"]);
            msg.Subject = "Ostruk Sergey";
            msg.SubjectEncoding = Encoding.UTF8;
            msg.Body = "EmailController";
            msg.BodyEncoding = Encoding.UTF8;
            msg.IsBodyHtml = false;
            msg.Priority = MailPriority.High;
            SmtpClient client = new SmtpClient();
            client.Credentials = new NetworkCredential
                (appSettings["smtpuser"], appSettings["smtppassword"]);
            client.Port = int.Parse(appSettings["smtpport"]);
            client.Host = appSettings["smtpserver"];
            client.EnableSsl = true;
            client.Send(msg);
            return Request.CreateResponse(HttpStatusCode.OK, "OK".ToString());
        }
    }
}