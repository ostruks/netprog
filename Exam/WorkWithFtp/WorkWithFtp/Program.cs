﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkWithFtp
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"D:\Ostruk\netprog\Exam\broadcast.txt";
            Console.WriteLine("Upload file? yes/no");
            string answer = Console.ReadLine();
            if(answer == "yes")
            {
                Console.WriteLine("Enter the full file name or press Enter");
                if(Console.ReadLine() != "")
                {
                    filename = Console.ReadLine();
                }
                else
                {
                    Console.WriteLine($"Default file = {filename}");
                }
                if (Upload(filename))
                {
                    string result = FtpLoader.DownloadFile(filename, @"D:\Ostruk\netprog\Exam\tmpinfo");
                    Console.WriteLine($"Download status = {result}");
                }
            }
            else
            {
                Console.WriteLine("As you want!");
            }
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
        private static bool Upload(string filename)
        {
            bool result = true;
            try
            {
                FtpLoader.UploadFile(filename);
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
    }
}
