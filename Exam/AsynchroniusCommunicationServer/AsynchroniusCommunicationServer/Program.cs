﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsynchroniusCommunicationServer
{
    class Program
    {
        public static List<Client> clients = new List<Client>();
        private static TcpListener serverSocket;
        private static NetworkStream networkStream;
        static int port = 55555; // порт для приема входящих запросов
        public static void BroadcastMessage(string msg, string uName, string friendName, bool flag, string id)
        {
            byte[] data = Encoding.ASCII.GetBytes($"{uName} says : {msg}");
            for (int i = 0; i < clients.Count; i++)
            {
                if (clients[i].ClientName != clients[i].FriendName)
                {
                    clients[i].Stream.Write(data, 0, data.Length);
                }
                else
                {
                    Program.BroadcastMessage(msg, uName, friendName, true, id);
                }
            }
        }
        static void Main(string[] args)
        {
            Task.Factory.StartNew(startServer);
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
        private static void startServer()
        {
            try
            {
                serverSocket = new TcpListener(IPAddress.Any, port);
                TcpClient clientSocket = default(TcpClient);
                int counter = 0;
                serverSocket.Start();
                Console.WriteLine("Chart Server started...");
                counter = 0;
                while (true)
                {
                    counter++;
                    clientSocket = serverSocket.AcceptTcpClient();
                    byte[] bytesForm = new byte[1024 * 10];
                    string dataFormClient = null;
                    networkStream = clientSocket.GetStream();
                    if ((networkStream.CanRead == false) || (networkStream.DataAvailable == false))
                    {
                        continue;
                    }
                    networkStream.Read(bytesForm, 0, bytesForm.Length);
                    dataFormClient = System.Text.Encoding.ASCII.GetString(bytesForm);
                    dataFormClient = dataFormClient.Substring(0, dataFormClient.LastIndexOf("$"));
                    Client client = new Client();
                    Console.WriteLine(dataFormClient.Split(':')[1] + " Joined chart room");
                    client.startClient(clientSocket, dataFormClient.Split(':')[1], dataFormClient.Split(':')[3]);
                    clients.Add(client);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                networkStream.Close();
                serverSocket.Stop();
            }
        }
    }
}
