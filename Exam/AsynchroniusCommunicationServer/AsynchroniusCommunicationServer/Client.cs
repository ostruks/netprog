﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsynchroniusCommunicationServer
{
    public class Client
    {
        private TcpClient clientSocket;
        private string clientName;
        private string friendName;
        private string id;
        private NetworkStream stream;
        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
        public NetworkStream Stream
        {
            get
            {
                return stream;
            }

            set
            {
                stream = value;
            }
        }
        public string ClientName
        {
            get
            {
                return clientName;
            }

            set
            {
                clientName = value;
            }
        }
        public string FriendName
        {
            get
            {
                return friendName;
            }

            set
            {
                friendName = value;
            }
        }
        public TcpClient ClientSocket
        {
            get
            {
                return clientSocket;
            }

            set
            {
                clientSocket = value;
            }
        }

        public void startClient(TcpClient inClientSocket, string clientName, string friendName)
        {
            this.id = Guid.NewGuid().ToString();
            this.clientSocket = inClientSocket;
            this.clientName = clientName;
            this.friendName = friendName;
            Thread ctThread = new Thread(doChat);
            ctThread.Start();
        }
        public Client(){}
        private void doChat()
        {
            byte[] bytesFrom = new byte[10025];
            string dataFromClient = null;
            while ((true))
            {
                try
                {
                    Stream = clientSocket.GetStream();
                    NetworkStream networkStream = clientSocket.GetStream();
                    if (networkStream.CanRead == false || networkStream.DataAvailable == false)
                        continue;
                    networkStream.Read(bytesFrom, 0, bytesFrom.Length);
                    dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
                    dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));
                    Console.WriteLine("From client - " + clientName + " : " + dataFromClient.Split(':')[4]);
                    Program.BroadcastMessage(dataFromClient.Split(':')[4], clientName, friendName, true, this.id);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
