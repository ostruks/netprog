﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AsynchroniusCommunication
{
    public partial class MainForm : Form
    {
        private TcpClient clientSocket;
        private NetworkStream serverStream = default(NetworkStream);
        private int ServerPort = 55555;
        private string ServerIp = GetLocalIP();
        private string readData = null;
        private Task ctThread;
        private CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
        
        public MainForm()
        {
            InitializeComponent();
            textLocalIp.Text = GetLocalIP();
            btnSend.Enabled = false;
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            connect();                     
        }
        private void btnSend_Click(object sender, EventArgs e)
        {
            byte[] outStream = System.Text.Encoding.ASCII.GetBytes($"{textLocalIp.Text}:{textLocalPort.Text}:{textFriendsIp.Text}:{textFriendsPort.Text}:" + textMessage.Text + "$");
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();
        }
        private void textMessage_TextChanged(object sender, EventArgs e)
        {
            if (textMessage.Text != "")
            {
                btnSend.Enabled = true;
            }
            else
            {
                btnSend.Enabled = false;
            }
        }
        private void textMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSend_Click(this, null);
            }
        }

        private static string GetLocalIP()
        {
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "127.0.0.1";
        }
        private void getMessage(CancellationToken token)
        {
            while (true)
            {
                try
                {
                    if (token.IsCancellationRequested)
                    {
                        MessageBox.Show("Operation is interrupted by a token");
                        return;
                    }
                    serverStream = clientSocket.GetStream();
                    int buffSize = 0;
                    byte[] inStream = new byte[1024*10];
                    buffSize = clientSocket.ReceiveBufferSize;
                    if (serverStream.CanRead == false || serverStream.DataAvailable == false)
                        continue;
                    serverStream.Read(inStream, 0, inStream.Length);
                    //
                    string returndata = System.Text.Encoding.ASCII.GetString(inStream);
                    readData = $"{returndata}";
                    msg();
                }
                catch (Exception) { }
            }
        }
        private void msg()
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(msg));
            else
                listMessage.Items.Add(Environment.NewLine + " >> " + readData);
        }
        private void connect()
        {
            try
            {              
                clientSocket = new TcpClient(new IPEndPoint(IPAddress.Parse(ServerIp), ServerPort));
                clientSocket.Connect(ServerIp, ServerPort);
                serverStream = clientSocket.GetStream();
                byte[] outStream = System.Text.Encoding.ASCII.
                    GetBytes($"{textLocalIp.Text}:{textLocalPort.Text}:{textFriendsIp.Text}:{textFriendsPort.Text}:$");
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
                CancellationToken token = cancelTokenSource.Token;
                ctThread = new Task(() => getMessage(token));
                ctThread.Start();
                readData = "Conected to Chat Server ...";
                msg();
                buttonStart.Text = "CONNECTED";
                buttonStart.Enabled = false;
                textMessage.Focus();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            cancelTokenSource.Cancel();
        }
    }
}
