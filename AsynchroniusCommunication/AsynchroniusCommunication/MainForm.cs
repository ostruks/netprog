﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AsynchroniusCommunication
{
    public partial class MainForm : Form
    {
        Socket sender;
        TcpClient clientSocket = new TcpClient();

        public MainForm()
        {
            InitializeComponent();
            btnSend.Enabled = false;
            textLocalIp.Text = GetLocalIP();
            textFriendsIp.Text = GetLocalIP();
        }
        private void buttonStart_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() => GoListenServer(Convert.ToInt32(textLocalPort.Text)));
            buttonStart.Enabled = false;                      
        }
        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                SendMessageFromSocket(textMessage.Text, Convert.ToInt32(textLocalPort.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void textMessage_TextChanged(object sender, EventArgs e)
        {
            if (textMessage.Text != "")
            {
                btnSend.Enabled = true;
            }
            else
            {
                btnSend.Enabled = false;
            }
        }
        private void SendMessageFromSocket(string message, int port)
        {
            IPHostEntry ipHost = Dns.GetHostEntry(textFriendsIp.Text);
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);
            sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            sender.Connect(ipEndPoint);
            byte[] bytes = new byte[1024 * 10];
            byte[] msg = Encoding.UTF8.GetBytes(message);
            int bytesSent = sender.Send(msg);
            int bytesRec = sender.Receive(bytes);
            if (message.IndexOf("stop") == -1)
            {               
                listMessage.Items.Add($"\nОтвет от сервера: {Encoding.UTF8.GetString(bytes, 0, bytesRec)}\n\n");
            }
            else
            {
                sender.Shutdown(SocketShutdown.Both);
                sender.Close();
            }            
        }
        #region Сервер
        private static string GetLocalIP()
        {
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "127.0.0.1";
        }
        public void GoListenServer(int port)
        {
            IPHostEntry ipHost = Dns.GetHostEntry(textLocalIp.Text);
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);
            Socket sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                sListener.Bind(ipEndPoint);
                sListener.Listen(10);
                while (true)
                {
                    Socket handler = sListener.Accept();
                    string data = null;
                    byte[] bytes = new byte[1024 * 10];
                    int bytesRec = handler.Receive(bytes);
                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                    string reply = data;
                    byte[] msg = Encoding.UTF8.GetBytes(reply);
                    handler.Send(msg);
                    if (data.IndexOf("stop") > -1)
                    {
                        listMessage.Invoke(new Action(() => { listMessage.Items.Add("Сервер завершил соединение с клиентом."); }));
                        btnSend.Invoke(new Action(() => { btnSend.Enabled = false; }));
                        break;
                    }
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion       
    }
}
