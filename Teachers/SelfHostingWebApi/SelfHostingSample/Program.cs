﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.Http.SelfHost;

namespace SelfHostingSample
{
    class Program
    {
        static void Main(string[] args)
        {
            string address = "http://localhost:8889";
            //это строка, которую нужно ввести в браузере
            //это адрес ведет к нашему серверу-хосту
            //все будет внутри нашего локального IIS

            //HttpSelfHostConfiguration - дает возможность задавать некоторые
            //настройки нашего сервера
            HttpSelfHostConfiguration config = new HttpSelfHostConfiguration(address);
            HttpSelfHostServer server = new HttpSelfHostServer(config,
                                                            new MyHttpMessageHandler());
            var task = server.OpenAsync();
            task.Wait();
            Console.WriteLine($"Сервер успешно запущено по адресу {address}");
            Console.WriteLine("Для завершения работы нажмти эникей");
            Console.ReadKey();


        }
    }
}
