﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;

namespace SelfHostingWithRoutingAndControllers
{
    public class MyController : ApiController
    {
        public string Get()
        {
            return "Ответ SelfHosting Web Api My Controller";
        }
    }
}
