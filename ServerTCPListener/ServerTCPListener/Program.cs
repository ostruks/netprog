﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.Net.Sockets;
using System.Collections;

namespace ServerTCPListener
{
    class Program
    {
        public static Hashtable clientsList = new Hashtable();

        public static void broadcast(string msg, string uName, bool flag)
        {
            foreach (DictionaryEntry Item in clientsList)
            {
                TcpClient broadcastSocket;
                broadcastSocket = (TcpClient)Item.Value;
                try
                {
                    NetworkStream broadcastStream = broadcastSocket.GetStream();
                    Byte[] broadcastBytes = null;
                    if (flag == true)
                    {
                        broadcastBytes = Encoding.ASCII.GetBytes(uName + " says : " + msg);
                    }
                    else
                    {
                        broadcastBytes = Encoding.ASCII.GetBytes(msg);
                    }
                    broadcastStream.Write(broadcastBytes, 0, broadcastBytes.Length);
                    broadcastStream.Flush();
                }
                catch (Exception) { }
            }
        }  //end broadcast function

        static void Main(string[] args)
        {
            TcpListener serverSocket = new TcpListener(6491);
            TcpClient clientSocket = default(TcpClient);
            int counter = 0;
            serverSocket.Start();
            Console.WriteLine("Chart Server started...");
            counter = 0;
            while (true)
            {
                counter++;
                clientSocket = serverSocket.AcceptTcpClient();
                byte[] bytesForm = new byte[1024*10];
                string dataFormClient = null;
                NetworkStream networkStream = clientSocket.GetStream();
                if((networkStream.CanRead == false) || (networkStream.DataAvailable == false))
                {
                    continue; //наследующий шаг - просто прослушивание, без обработки данных
                }
                networkStream.Read(bytesForm, 0, bytesForm.Length); //читаю всегда весь буффер
                // и так как есть маркер - - признак окончания смысловой части
                dataFormClient = System.Text.Encoding.ASCII.GetString(bytesForm);
                dataFormClient = dataFormClient.Substring(0, dataFormClient.LastIndexOf("$"));
                clientsList.Add(dataFormClient, clientSocket);
                Console.WriteLine(dataFormClient + " Joined chart room");
                handleClient client = new handleClient();
                client.startClient(clientSocket, dataFormClient, clientsList);
            }
        }
    }

    public class handleClient
    {
        TcpClient clientSocket;
        string clNo;
        Hashtable clientsList;

        public void startClient(TcpClient inClientSocket, string clineNo, Hashtable cList)
        {
            this.clientSocket = inClientSocket;
            this.clNo = clineNo;
            this.clientsList = cList;
            Thread ctThread = new Thread(doChat);
            ctThread.Start();
        }

        private void doChat()
        {
            int requestCount = 0;
            byte[] bytesFrom = new byte[10025];
            string dataFromClient = null;
            Byte[] sendBytes = null;
            string serverResponse = null;
            string rCount = null;
            requestCount = 0;
            while ((true))
            {
                try
                {
                    requestCount = requestCount + 1;
                    NetworkStream networkStream = clientSocket.GetStream();
                    //networkStream.Read(bytesFrom, 0, (int)clientSocket.ReceiveBufferSize);
                    //
                    if (networkStream.CanRead == false || networkStream.DataAvailable == false)
                        continue;
                    networkStream.Read(bytesFrom, 0, bytesFrom.Length);
                    //
                    dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
                    dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));
                    Console.WriteLine("From client - " + clNo + " : " + dataFromClient);
                    rCount = Convert.ToString(requestCount);
                    Program.broadcast(dataFromClient, clNo, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }//end while
        }//end doChat
    } //end class handleClinet
}
