﻿using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.ComponentModel;
using System.Configuration;
using System.Collections.Specialized;
using System.Text;

namespace TestMailDelivery
{
    public static class SmtpHelper
    {
        public static void SendEmail(string to, string subject, string body)
        {
            MailMessage msg = new MailMessage();

            NameValueCollection appSettings = ConfigurationManager.AppSettings;

            msg.Attachments.Add(new Attachment(@"D:/MyData/log.txt")); //Отправка файла
            msg.To.Add(to);
            msg.From = new MailAddress(appSettings["smtpuser"]);
            msg.Subject = subject;
            msg.SubjectEncoding = Encoding.UTF8;
            msg.Body = body;
            msg.BodyEncoding = Encoding.UTF8;
            msg.IsBodyHtml = false;
            msg.Priority = MailPriority.High; //Приоритет письма

            //Add the Creddentials
            SmtpClient client = new SmtpClient();
            client.Credentials = new NetworkCredential
                (appSettings["smtpuser"], appSettings["smtppassword"]);
            client.Port = int.Parse(appSettings["smtpport"]);
            client.Host = appSettings["smtpserver"];
            client.EnableSsl = true;
            client.Send(msg);
        }
    }
}
