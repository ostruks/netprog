﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using System.IO;
using System.Net;
using System.Net.Sockets;
//

namespace SocketServer
{
    class Program
    {
        private static string GetLocalIP()
        {
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "127.0.0.1";
        }

        /// <summary>
        /// перевод строки с hex содержимым в байты - работает со счетчиками Энергомер-ы
        /// </summary>
        //byte[] msg = Encoding.UTF8.GetBytes(reply);// Encoding.ASCII.GetBytes(hexStr0);
        //где s должен быть в виде "C8 CA CB CC CD 0C 00 08 00 24 67 70 33 3d 30 0D 0D 0A";
        public static byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        static void Main(string[] args)
        {
            // Устанавливаем для сокета локальную конечную точку
            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            //IPHostEntry ipHost = Dns.GetHostEntry(GetLocalIP());
            //IPHostEntry ipHost = Dns.GetHostEntry("192.168.1.34");
            //IPEndPoint ipHost = new IPEndPoint(new IPAddress( new byte[] {159,224,77,199 }) , 5000);
            //IPHostEntry ipHost = Dns.GetHostEntry(getIpAddressFromFile("ipaddres.txt"));
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 5000);
            // Создаем сокет Tcp/Ip
            Socket sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            // Назначаем сокет локальной конечной точке и слушаем входящие сокеты
            try
            {
                sListener.Bind(ipEndPoint);
                sListener.Listen(10);

                // Начинаем слушать соединения
                while (true)
                {
                    Console.WriteLine("Ожидаем соединение через порт {0}", ipEndPoint);

                    // Программа приостанавливается, ожидая входящее соединение
                    Socket handler = sListener.Accept();
                    string data = null;

                    // Мы дождались клиента, пытающегося с нами соединиться

                    byte[] bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);

                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);

                    // Показываем данные на консоли
                    Console.Write("Полученный текст: " + data + "\n\n");

                    // Отправляем ответ клиенту\
                    //string reply = "Спасибо за запрос в " + data.Length.ToString() + " символов.Сообщение: "+ data;
                    string reply = data;
                    //byte[] msg = Encoding.UTF8.GetBytes(reply);
                    byte[] msg = HexStringToByteArray(reply);
                    handler.Send(msg);

                    if (data.IndexOf("<TheEnd>") > -1)
                    {
                        Console.WriteLine("Сервер завершил соединение с клиентом.");
                        break;
                    }

                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
            //
        }
    }
}
