﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace WebApiTest.Controllers
{
    public class SecondController : ApiController
    {
        // Метод сработает при получении GET запроса.
        [HttpGet] // альтернатива [AcceptVerbs("GET")]
        public string SayHello()
        {
            return "Сработал метод SayHello c атрибутом HttpGet";
        }
        #region другой способ
        //[AcceptVerbs("GET")] //альетернатива [AcceptVerbs("GET")]
        //public string SayHello2()
        //{
        //    return "Сработал метод SayHello c атрибутом HttpGet";
        //}
        #endregion
    }
}