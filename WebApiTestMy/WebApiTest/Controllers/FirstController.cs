﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace WebApiTest.Controllers
{
    public class FirstController : ApiController
    {
        // Метод сработает при получении GET запроса.
        public string Get()
        {
            return "Сработал метод Get";
        }
    }  
}