﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HttpClientTest
{
    //паттерн проектирования Command (версия с++)
    abstract class Action
    {
        public abstract void Invoke();
    }
}
