﻿namespace HttpClientTest
{
    //Паттерн проектирования Command (версия C++)
    abstract class Action
    {
        public abstract void Invoke();
    }
}
