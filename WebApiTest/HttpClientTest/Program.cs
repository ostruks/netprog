﻿using System;

namespace HttpClientTest
{
    class Program
    {
        public static string URI = "http://localhost:1578/api/tasks";
        static void Main(string[] args)
        {
            start:
            Console.WriteLine("Введите тип запроса: GET, POST, PUT, DELETE");
            string command = Console.ReadLine().ToUpper();
            switch (command)
            {
                case "GET":
                    new GetAction().Invoke();
                    break;
                case "EXIT":
                    return;
            }
            goto start;
        }
        public static void PrintError(string msg)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(msg);
            Console.ResetColor();
        }
    }
}
