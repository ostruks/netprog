﻿using System.Collections.Generic;
using WebApiTestLibrary;

namespace WebApiTest.Models
{
    public class TasksDataSource
    {
        private static List<WebApiTestLibrary.Task> _tasks = null;

        public static List<WebApiTestLibrary.Task> All
        {
            get
            {
                if (_tasks == null)
                {
                    _tasks = new List<Task>();
                    _tasks.Add(new Task() { ID = 1, Name = "Купить СЛОНА", IsCompleted = false });
                    _tasks.Add(new Task() { ID = 2, Name = "Спасти мир", IsCompleted = false });
                    _tasks.Add(new Task() { ID = 3, Name = "Пообедать", IsCompleted = false });
                }
                return _tasks;
            }
        }
    }
}