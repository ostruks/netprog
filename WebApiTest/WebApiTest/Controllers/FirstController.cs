﻿using System.Web.Http;

namespace WebApiTest.Controllers
{
    public class FirstController : ApiController
    {
        // Метод сработает при получении GET запроса.
        public string Get()
        {
            return "Сработал метод Get";
        }
    }
}