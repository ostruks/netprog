﻿using System.Web.Http;

namespace WebApiTest.Controllers
{
    public class SecondController : ApiController
    {
        //Метод сработает при получении GET запроса.

        [HttpGet]
        public string SayHello()
        {
            return "Сработал метод SayHello c атрибутом HttpGet";
        }
        #region Другой способ
        //[AcceptVerbs("GET")]
        //public string SayHello2()
        //{
        //    return "Сработал метод SayHello c атрибутом HttpGet";
        //}
        #endregion
    }
}