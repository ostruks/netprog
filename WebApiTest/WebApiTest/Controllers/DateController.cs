﻿using System;
using System.Web.Http;

namespace WebApiTest.Controllers
{
    public class DateController : ApiController
    {
        // GET api/date
        public string Get()
        {
            return DateTime.Now.ToString();
        }
    }
}
