﻿using System.Web.Http;

namespace WebApiTest.Controllers
{
    public class ThirdController : ApiController
    {
        // Метод сработает при получении GET запроса.
        public string GetHelloString()
        {
            return "Сработал метод GetHelloString";
        }
        // Метод сработает при получении POST запроса.
        public string PostHelloString()
        {
            return "Сработал метод PostHelloString";
        }
    }
}