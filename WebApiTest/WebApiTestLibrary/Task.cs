﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiTestLibrary
{
    public class Task
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsCompleted { get; set; }
        public Task() { }
    }
}
